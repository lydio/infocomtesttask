//
//  MainMenuPresenter.swift
//  InfocomTestTask
//
//  Created by Admin on 06.08.2021.
//

import Foundation

protocol MainMenuViewProtocol: class {
    func performMenuChanges(sections: [MainMenuSectionModel])
}

class MainMenuPresenter {
    
    weak var view:MainMenuViewProtocol? = nil {
        didSet {
            view?.performMenuChanges(sections: model.sections)
        }
    }
    private var model = MainMenuModel()
    
    func resetUserMenuItems(user:UserModel) {
        view?.performMenuChanges(sections: model.sections(forUser: user))
    }
    
    func removeUserMenuItems() {
        view?.performMenuChanges(sections: model.sections)
    }
    
}
