//
//  Alerts.swift
//  InfocomTestTask
//
//  Created by Admin on 07.08.2021.
//

import UIKit

class AlertHelper {
    
    private static func presentingViewController() -> UIViewController {
        return SplitViewController.shared
    }
    
    static func showMessage(_ message:String) {
      
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Close", style: .default, handler: nil)
        alert.addAction(action)
        presentingViewController().present(alert, animated: true)
    }
    
    static func showConfirmationMessage(_ message:String, handler: @escaping ()->()) {
      
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (_) in
            handler()
        })
        alert.addAction(okAction)
        presentingViewController().present(alert, animated: true)
    }
    
    static func showOkMessage(_ message:String, handler: @escaping ()->()) {
      
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (_) in
            handler()
        })
        alert.addAction(okAction)
        presentingViewController().present(alert, animated: true)
    }
}
