//
//  InfocomTestTaskUITests3.swift
//  InfocomTestTaskUITests
//
//  Created by lydio on 8/27/21.
//

import XCTest

class InfocomTestTaskUITests3: XCTestCase {

    override func setUpWithError() throws {
        
        let app = XCUIApplication()
        app.launch()
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {
       
        let app = XCUIApplication()
        app.terminate()
    }
    
    func testExample1() throws {
        
        let app = XCUIApplication()
        let menuButton = app.navigationBars["Home"].buttons["Menu"]
        menuButton.tap()
        
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Locations"]/*[[".cells.staticTexts[\"Locations\"]",".staticTexts[\"Locations\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.navigationBars["Locations"].buttons["Menu"].tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Contacts"]/*[[".cells.staticTexts[\"Contacts\"]",".staticTexts[\"Contacts\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.navigationBars["Contacts"].buttons["Menu"].tap()
        //tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Home"]/*[[".cells.staticTexts[\"Home\"]",".staticTexts[\"Home\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Login"]/*[[".cells.staticTexts[\"Login\"]",".staticTexts[\"Login\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        app.textFields.element(boundBy: 0).tap()
        app.keys["u"].tap()
        app.keys["a"].tap()
        app.secureTextFields.element(boundBy: 0).tap()
        app.keys["q"].tap()
        app.keys["w"].tap()
        app.keys["e"].tap()
        app/*@START_MENU_TOKEN@*/.staticTexts["LOGIN"]/*[[".buttons[\"LOGIN\"].staticTexts[\"LOGIN\"]",".staticTexts[\"LOGIN\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.alerts.scrollViews.otherElements.buttons["Close"].tap()
        
        sleep(1)
    }
    
    
    func testExample2() throws {
        
        let app = XCUIApplication()
        let menuButton = app.navigationBars["Home"].buttons["Menu"]
        menuButton.tap()
        
        var tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Login"]/*[[".cells.staticTexts[\"Login\"]",".staticTexts[\"Login\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app/*@START_MENU_TOKEN@*/.staticTexts["LOGIN"]/*[[".buttons[\"LOGIN\"].staticTexts[\"LOGIN\"]",".staticTexts[\"LOGIN\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        tablesQuery = app.tables
        let _ = tablesQuery.firstMatch.waitForExistence(timeout: 60)
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["7562 5674 1380 4726"]/*[[".cells.staticTexts[\"7562 5674 1380 4726\"]",".staticTexts[\"7562 5674 1380 4726\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        app.scrollViews.buttons.firstMatch.tap()
        app.textFields.element(boundBy: 2).tap()
        app.keys["1"].tap()
        app.keys["0"].tap()
        app.keys["0"].tap()
        app.staticTexts["SEND"].tap()
        app.alerts.scrollViews.otherElements.buttons["Close"].tap()
        app.staticTexts["HISTORY"].tap()
        app.navigationBars["Transaction History"].buttons["Account"].tap()
        app.navigationBars["Account"].buttons["Menu"].tap()
        
        sleep(1)
    }
    
    func testExample3() throws {
        
        let app = XCUIApplication()
        let menuButton = app.navigationBars["Home"].buttons["Menu"]
        menuButton.tap()
        
        var tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Login"]/*[[".cells.staticTexts[\"Login\"]",".staticTexts[\"Login\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app/*@START_MENU_TOKEN@*/.staticTexts["LOGIN"]/*[[".buttons[\"LOGIN\"].staticTexts[\"LOGIN\"]",".staticTexts[\"LOGIN\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        tablesQuery = app.tables
        let _ = tablesQuery.firstMatch.waitForExistence(timeout: 60)
        app.tables/*@START_MENU_TOKEN@*/.staticTexts["Test User"]/*[[".cells.staticTexts[\"Test User\"]",".staticTexts[\"Test User\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        app.scrollViews.otherElements/*@START_MENU_TOKEN@*/.staticTexts["EDIT"]/*[[".buttons[\"EDIT\"].staticTexts[\"EDIT\"]",".staticTexts[\"EDIT\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.alerts.scrollViews.otherElements.buttons["Close"].tap()
        app.scrollViews.otherElements/*@START_MENU_TOKEN@*/.staticTexts["LOGOUT"]/*[[".buttons[\"LOGOUT\"].staticTexts[\"LOGOUT\"]",".staticTexts[\"LOGOUT\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.alerts.scrollViews.otherElements.buttons["Ok"].tap()
                
        sleep(1)
    }

}
