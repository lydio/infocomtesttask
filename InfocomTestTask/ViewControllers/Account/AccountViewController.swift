//
//  AccountViewController.swift
//  InfocomTestTask
//
//  Created by Admin on 08.08.2021.
//


import UIKit

class AccountViewController: ContentViewController {

    let presenter = AccountPresenter()
    
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var stackView: UIStackView!
    private weak var balanceLabel:FieldLabel?
    
    override func viewDidLoad() {
        navigationTitle = "Account"
        super.viewDidLoad()
        
        presenter.view = self
    }
    
    override func viewWillLayoutSubviews(){
        super.viewWillLayoutSubviews()
        
        scrollView.contentSize = stackView.bounds.size
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        updateBalanceLabel()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? TransactionViewController {
            vc.presenter.selectedAccountItem = presenter.selectedAccountItem
        
        } else if let vc = segue.destination as? TransactionHistoryViewController {
            vc.presenter.selectedAccountItem = presenter.selectedAccountItem
        }
    }
}


extension AccountViewController: AccountViewProtocol {
    
    func showAccount(account: AccountModel) {
        
        let labelPrototype = stackView.arrangedSubviews.compactMap{ $0 as? FieldLabel }.first!
        
        let balanceName = account.balance.name
        var mainInfo = labelPrototype.spawn(withFields: account.mainInfo())
        mainInfo.removeFirst()
        var index = 1
        for label in mainInfo {
            if label.name == balanceName {
                balanceLabel = label
            }
            stackView.insertArrangedSubview(label, at: index)
            index += 1
        }
    }
    
    func updateBalanceLabel() {
        
        if let balance = presenter.balance {
            balanceLabel?.value = balance
        }
    }
}


