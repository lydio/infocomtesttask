//
//  PersonalDataModel.swift
//  InfocomTestTask
//
//  Created by Admin on 08.08.2021.
//

import Foundation

struct PersonalDataModel {
    
    var firstName:FieldModel!
    var lastName:FieldModel!
    var birth:FieldModel!
    var gender:FieldModel!
    var country:FieldModel!
    var city:FieldModel!
    var address:FieldModel!
    var zip:FieldModel!
    
    var locale:FieldModel?
    var phone:FieldModel?
    var mobile:FieldModel?
    var email:FieldModel!
    
    func identity() -> [FieldModel] {
        return [firstName, lastName, birth, gender]
    }
    func location() -> [FieldModel] {
        return [country, city, address, zip]
    }
    func contactInfo() -> [FieldModel] {
        
        var fields = [FieldModel]()
        
        if let locale = locale {
            fields.append(locale)
        }
        if let phone = phone {
            fields.append(phone)
        }
        if let mobile = mobile {
            fields.append(mobile)
        }
        fields.append(email)
        return fields
    }
    
}
