//
//  TransactionViewController.swift
//  InfocomTestTask
//
//  Created by Admin on 08.08.2021.
//

import UIKit
import ProgressHUD


class TransactionViewController: ContentViewController {

    let presenter = TransactionPresenter()
    
    @IBOutlet weak var accountNumberTextField: UITextField!
    @IBOutlet weak var otherAccountNumberTextField: UITextField!
    @IBOutlet weak var sumTextField: UITextField!
    
    override func viewDidLoad() {
        navigationTitle = "Transaction"
        super.viewDidLoad()
        
        presenter.view = self
        
        otherAccountNumberTextField.text = "1111 2222 3333 4444"
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        ProgressHUD.dismiss()
        presenter.cancelAllTasks()
    }
    
    @IBAction func sendTransactionReqest(_ sender: Any) {

        guard
            let otherAccountNumber = otherAccountNumberTextField.text,
            presenter.checkAccountNumberInput(accountNumber: otherAccountNumber)
        else {
            AlertHelper.showMessage("Invalid account number!")
            return
        }
        guard let sum = sumTextField.text, let f = Float(sum), f > 0 else {
            AlertHelper.showMessage("Invalid sum!")
            return
        }
        
        
        ProgressHUD.show()
        presenter.sendTransactionRequest(accountNumber: accountNumberTextField.text!, otherAccountNumber: otherAccountNumber, sum: sum)
    }
    
    
    @IBAction func backgroundTap(_ sender: Any) {
        
        accountNumberTextField.resignFirstResponder()
        otherAccountNumberTextField.resignFirstResponder()
        sumTextField.resignFirstResponder()
    }
}


extension TransactionViewController: TransactionViewProtocol {
    
    func fillForm(forAccount account: AccountModel) {
        accountNumberTextField.text = account.number.value
    }
    
    func showTransactionSuccess(user:TransactionModel) {
        
        ProgressHUD.dismiss()
        navigationController?.popViewController(animated: true)
        AlertHelper.showMessage("Transaction completed")
    }
    
    func showTransactionFailure(error:String) {
        
        ProgressHUD.dismiss()
        AlertHelper.showMessage(error)
    }
}



