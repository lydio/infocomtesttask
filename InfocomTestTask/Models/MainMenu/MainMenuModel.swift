//
//  MainMenuModel.swift
//  InfocomTestTask
//
//  Created by Admin on 06.08.2021.
//

import UIKit


class MainMenuModel {
    
    private(set) var sections:[MainMenuSectionModel] = []
    
    init() {
        let path = Bundle.main.path(forResource: "MainMenuModel", ofType: "plist")!
        let array = NSArray(contentsOfFile: path) as! Array<Dictionary<String, Any>>
        //print(array)
        
        for dict in array {
            var section = MainMenuSectionModel()
            if let menuTitle = dict["menuTitle"] as? String, menuTitle.count > 0 {
                section.menuTitle = menuTitle
            }
            if let array = dict["items"] as? Array<Dictionary<String, Any>> {
                for dict in array {
                    var item = MainMenuItemModel()
                    item.menuTitle = dict["menuTitle"] as? String
                    item.contentIdentifier = dict["contentIdentifier"] as? String
                    
                    if let menuIconImage = dict["menuIconImage"] as? String {
                        item.menuIconImage = UIImage(named: menuIconImage)
                    }
                    section.items.append(item)
                }
            }
            sections.append(section)
        }
        //print(sections)
    }
    
    func sections(forUser user:UserModel) -> [MainMenuSectionModel] {
        
        let personalDataSectionIndex = 0
        let accountsSectionIndex = 2
        
        var sectionsArray = [MainMenuSectionModel]()
        sectionsArray.append(contentsOf: sections)
        
        let personalData = user.personalData!
        
        var menuTitle = "Unknown"
        if let firstName = personalData.firstName.value {
            menuTitle = firstName
            if let lastName = personalData.lastName.value {
                menuTitle += (" " + lastName)
            }
        }
        
        let menuIconImage = user.photo ?? sections[personalDataSectionIndex].items[0].menuIconImage
        let personalDataItem = MainMenuItemModel(menuTitle: menuTitle, menuIconImage: menuIconImage, contentIdentifier: "User")
        sectionsArray[personalDataSectionIndex].items[0] = personalDataItem
        
        for account in user.accounts {
            if let menuTitle = account.number.value, let curency = account.currency.value {
                let accountItem = MainMenuItemModel(menuTitle: menuTitle, menuIconImage: currencyIconImage(curency), contentIdentifier: "Account")
                sectionsArray[accountsSectionIndex].items.append(accountItem)
            }
        }
        
        return sectionsArray
    }
    
    private func currencyIconImage(_ currency:String) -> UIImage {
        
        switch currency.lowercased() {
        case "dollar":
            return UIImage(named: "dollar-symbol")!
        case "euro":
            return UIImage(named: "euro-currency-symbol")!
        default:
            return UIImage(named: "question-sign")!
        }
    }
}

struct MainMenuSectionModel {
    
    var menuTitle:String?
    var items:[MainMenuItemModel] = []
}

struct MainMenuItemModel {
    
    var menuTitle:String!
    var menuIconImage:UIImage?
    var contentIdentifier:String!
}

