//
//  AuthService.swift
//  InfocomTestTask
//
//  Created by Admin on 08.08.2021.
//

import Foundation


class AuthService {
    
    static let logoutNotification = "Logout"
    
    static func logout() {
        UserModel.currentUser = nil
        NotificationCenter.default.post(name: Notification.Name(logoutNotification), object: nil, userInfo: nil)
        //TODO: Network manager
    }
    
    static func login(email:String, password:String, completion: @escaping (UserModel?, String?)->()) {
        
        //TODO: Network manager
        DispatchQueue.global().asyncAfter(deadline: .now() + 1.0) {
            let user = UserModel.loadTestUserFromFile(email: email, password: password)
            
            DispatchQueue.main.async {
                if let user = user {
                    UserModel.currentUser = user
                    completion(user, nil)
                } else {
                    UserModel.currentUser = nil
                    completion(nil, "Invalid password or email")
                }
            }
        }
    }
    
}
