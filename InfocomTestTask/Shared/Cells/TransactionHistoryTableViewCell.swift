//
//  TransactionHistoryTableViewCell.swift
//  InfocomTestTask
//
//  Created by Admin on 09.08.2021.
//

import UIKit

class TransactionHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var createdAtLabel:UILabel!
    @IBOutlet weak var otherAccountNumnberLabel:UILabel!
    @IBOutlet weak var sumLabel:UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
