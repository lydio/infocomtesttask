//
//  UserViewController.swift
//  InfocomTestTask
//
//  Created by Admin on 07.08.2021.
//


import UIKit

class UserViewController: ContentViewController {

    private let presenter = UserPresenter()
    
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet private weak var imageView: UIImageView!
    
    
    override func viewDidLoad() {
        navigationTitle = "User"
        super.viewDidLoad()
        
        presenter.view = self
    }
    
    override func viewWillLayoutSubviews(){
        super.viewWillLayoutSubviews()
        
        scrollView.contentSize = stackView.bounds.size
    }
    
    @IBAction private func logout(_ sender: Any) {
        AlertHelper.showConfirmationMessage("Are you sure you want to log out?") { [unowned self] in
            self.presenter.logout()
        }
    }
    
    @IBAction private func edit(_ sender: Any) {
        AlertHelper.showMessage("In Development")
    }
}


extension UserViewController: UserViewProtocol {
    
    func showUser(personalData: PersonalDataModel, photo:UIImage?) {
        
        let labelPrototype = stackView.arrangedSubviews.compactMap{ $0 as? FieldLabel }.first!
        var identity = labelPrototype.spawn(withFields: personalData.identity())
        identity.removeFirst()
        var index = 2
        for label in identity {
            stackView.insertArrangedSubview(label, at: index)
            index += 1
        }
        let location = labelPrototype.clone().spawn(withFields: personalData.location())
        for label in location {
            stackView.insertArrangedSubview(label, at: index)
            index += 1
        }
        let contactInfo = labelPrototype.clone().spawn(withFields: personalData.contactInfo())
        for label in contactInfo {
            stackView.insertArrangedSubview(label, at: index)
            index += 1
        }
        //print(identity, location, contactInfo)
        imageView.image = photo
    }
}


