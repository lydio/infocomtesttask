//
//  DateFormatter+Extensions.swift
//  InfocomTestTask
//
//  Created by Admin on 08.08.2021.
//

import Foundation


extension DateFormatter {
    
    func currentYYYYMMDD() -> String {
        dateFormat = "yyyy-MM-dd"
        return string(from: Date())
    }
}
