//
//  FieldModel.swift
//  InfocomTestTask
//
//  Created by Admin on 08.08.2021.
//

import Foundation

struct FieldModel {
    var name:String!
    var value:String?
    var required:String?
}
