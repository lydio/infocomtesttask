//
//  TransactionHistoryPresenter.swift
//  InfocomTestTask
//
//  Created by Admin on 08.08.2021.
//


import Foundation


protocol TransactionHistoryViewProtocol:class {
    func showHistory(forAccount account:AccountModel)
}

class TransactionHistoryPresenter {
    
    weak var view:TransactionHistoryViewProtocol? = nil {
        didSet {
            assert(selectedAccountItem >= 0 && selectedAccountItem < model.accounts.count)
            view?.showHistory(forAccount: model.accounts[selectedAccountItem])
        }
    }
    
    var selectedAccountItem:Int = -1
    
    private var model:UserModel {
        return UserModel.currentUser!
    }
}

