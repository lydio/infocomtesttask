// This file contains the fastlane.tools configuration
// You can find the documentation at https://docs.fastlane.tools
//
// For a list of all available actions, check out
//
//     https://docs.fastlane.tools/actions
//


import Foundation

class Fastfile: LaneFile {
    
    private let projectName = "InfocomTestTask"
    
    private func incrementVersionOrError(_ options:[String: String]?) -> String? {
        
        if let appVersion = options?["appVersion"], appVersion.count > 0 {
            let components = appVersion.split(separator: ".")
            for c in components {
                if Int(c) == nil {
                    return "Invalid appVersion: \(appVersion)"
                }
            }
            incrementVersionNumber(versionNumber: OptionalConfigValue(stringLiteral: appVersion), xcodeproj: "\(projectName).xcodeproj")
        }
        if let bumpType = options?["bumpType"] {
            switch bumpType {
                case "major", "minor", "patch":
                    incrementVersionNumber(bumpType: bumpType, xcodeproj: "\(projectName).xcodeproj")
                default:
                    return "Invalid bumpType: \(bumpType)"
            }
        }
        return nil
    }
	
    private func getScheme(_ options:[String: String]?) -> OptionalConfigValue<String?> {
        
        if let optScheme = options?["scheme"], optScheme.count > 0 {
            return OptionalConfigValue(stringLiteral: optScheme)
        }
        return OptionalConfigValue(stringLiteral: "InfocomTestTask")
    }
    
    private func getAppIdentifier(_ options:[String: String]?) -> OptionalConfigValue<String?> {
        if let optAppIdentifier = options?["appIdentifier"], optAppIdentifier.count > 0 {
            
            return OptionalConfigValue(stringLiteral: optAppIdentifier)
        }
        return OptionalConfigValue(stringLiteral: appIdentifier)
    }
    
    func betaLane(withOptions options:[String: String]?) {
    
        if let error = incrementVersionOrError(options) {
            echo(message: "\(error)")
            return
        }
        
        desc("Push a new beta build to TestFlight")
        
        buildApp(workspace: "\(projectName).xcworkspace", scheme: getScheme(options))
		
        uploadToTestflight(username: OptionalConfigValue(stringLiteral: appleID), appIdentifier: getAppIdentifier(options), teamId: itcTeam)
        
    }
    
}

