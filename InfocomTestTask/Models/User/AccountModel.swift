//
//  AccountModel.swift
//  InfocomTestTask
//
//  Created by Admin on 08.08.2021.
//

import Foundation

struct AccountModel {
    
    var number:FieldModel!
    var currency:FieldModel!
    var createdAt:FieldModel!
    var balance:FieldModel!
    var credit:FieldModel?
    var transactions:[TransactionModel] = []
    
    func mainInfo() -> [FieldModel] {
        return [number, currency, createdAt, balance]
    }
}
