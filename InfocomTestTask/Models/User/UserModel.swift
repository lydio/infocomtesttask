//
//  UserModel.swift
//  InfocomTestTask
//
//  Created by Admin on 08.08.2021.
//

import UIKit

class UserModel {
    
    //TODO: data manager
    static var currentUser:UserModel? = nil
    
    var personalData:PersonalDataModel!
    var accounts:[AccountModel] = []
    var photo:UIImage? = nil

    //todo DataManager
    // UserModel.loadTestUserFromFile(email:"user@gmail.com", password:"1234")
    static func loadTestUserFromFile(email:String, password:String) -> UserModel? {
        
        let path = Bundle.main.path(forResource: "TestUserModel", ofType: "plist")!
        let dict = NSDictionary(contentsOfFile: path) as! Dictionary<String, Any>
        
        let personalDataDict = dict["personalData"] as! Dictionary<String, Any>
        let accountsArray =  dict["accounts"] as! Array<Dictionary<String, Any>>
        let photoName = dict["photo"] as! String
        let hash = dict["hash"] as! String
        //print(personalDataDict, accountsArray, photoName, hash)
        
        let user = UserModel()
        user.photo = UIImage(named: photoName)
        
        func field(from obj:Any?) -> FieldModel? {
            guard let dict = obj as? Dictionary<String, String> else { return nil }
            return FieldModel(name: dict["name"], value: dict["value"], required: dict["required"])
        }
        var personalData = PersonalDataModel()
        personalData.firstName = field(from: personalDataDict["firstName"])!
        personalData.lastName = field(from: personalDataDict["lastName"])!
        personalData.birth = field(from: personalDataDict["birth"])!
        personalData.gender = field(from: personalDataDict["gender"])!
        personalData.country = field(from: personalDataDict["country"])!
        personalData.city = field(from: personalDataDict["city"])!
        personalData.address = field(from: personalDataDict["address"])!
        personalData.zip = field(from: personalDataDict["zip"])!
        personalData.birth = field(from: personalDataDict["birth"])!
        personalData.locale = field(from: personalDataDict["locale"])
        personalData.phone = field(from: personalDataDict["phone"])
        personalData.mobile = field(from: personalDataDict["mobile"])
        personalData.email = field(from: personalDataDict["email"])!
        user.personalData = personalData
        
        for accountDict in accountsArray {
            var account = AccountModel()
            account.number = field(from: accountDict["number"])!
            account.currency = field(from: accountDict["currency"])!
            account.createdAt = field(from: accountDict["createdAt"])!
            account.balance = field(from: accountDict["balance"])!
            account.credit = field(from: accountDict["credit"])
            user.accounts.append(account)
        }
        //print(user)
        guard personalData.email.value == email, hash == String(password.hash) else {
            return nil
        }
        return user
    }
    
    func executeTestTransaction(accountNumber:String, otherAccountNumber:String, sum:String) -> TransactionModel? {
        
        for i in 0..<accounts.count {
            if accounts[i].number.value == accountNumber {
                let newBalance = Float(accounts[i].balance.value!)! - Float(sum)!
                if newBalance >= 0 {
                    accounts[i].balance.value = "\(newBalance)"
                    let createdAtField = FieldModel(name: "Created at", value: DateFormatter().currentYYYYMMDD(), required: nil)
                    let sumField = FieldModel(name: "Sum", value: sum, required: nil)
                    let otherAccountNumberField = FieldModel(name: "Other Account", value: otherAccountNumber, required: nil)
                    let transaction = TransactionModel(createdAt: createdAtField, sum: sumField, otherAccountNumber: otherAccountNumberField)
                    accounts[i].transactions.append(transaction)
                    return transaction
                }
            }
        }
        return nil
    }
}
