//
//  ContentViewController.swift
//  InfocomTestTask
//
//  Created by Admin on 05.08.2021.
//

import UIKit

class ContentViewController: UIViewController {

    var navigationTitle: String = "#"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = navigationTitle
        self.view.backgroundColor = .white
        
        //if let splitController = self.splitViewController{
        //   if let navController = splitController.viewControllers.last as? UINavigationController {
        //      navController.topViewController?.navigationItem.leftBarButtonItem = splitController.displayModeButtonItem
        //   }
        //}
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        view.endEditing(true)
    }
}


extension ContentViewController {
    
    static func instantiateHome() -> ContentViewController {
        return instantiate(withIdentifier: "Home")
    }
    
    static func instantiateUser() -> ContentViewController {
        return instantiate(withIdentifier: "User")
    }
    
    static func instantiate(withIdentifier identifier:String) -> ContentViewController {
        
        let masterController = SplitViewController.shared.masterController
        
        switch identifier {
            case "Locations", "Contacts":
                return instantiateStub(identifier)
            case "Login":
                let vc = UIStoryboard(name: identifier, bundle: nil).instantiateInitialViewController() as! LoginViewController
                vc.presenter.view = masterController as? LoginViewProtocol
                assert(vc.presenter.view != nil)
                return vc
            default:
                return UIStoryboard(name: identifier, bundle: nil).instantiateInitialViewController() as! ContentViewController
        }
    }
    
    private static func instantiateStub(_ identifier:String) -> ContentViewController {
        
        let vc = ContentViewController()
        vc.navigationTitle = identifier
        
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 21)
        label.textColor = .red
        label.text = "In Development"
        label.translatesAutoresizingMaskIntoConstraints = false
        vc.view.addSubview(label)
        
        NSLayoutConstraint(item: label, attribute: .centerX, relatedBy: .equal, toItem: vc.view, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: label, attribute: .centerY, relatedBy: .equal, toItem: vc.view, attribute: .centerY, multiplier: 1, constant: 0).isActive = true
        return vc
    }
}
