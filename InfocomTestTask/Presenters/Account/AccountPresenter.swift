//
//  AccountPresenter.swift
//  InfocomTestTask
//
//  Created by Admin on 07.08.2021.
//

import Foundation


protocol AccountViewProtocol:class {
    func showAccount(account:AccountModel)
}

class AccountPresenter {
    
    weak var view:AccountViewProtocol? = nil {
        didSet {
            assert(selectedAccountItem >= 0 && selectedAccountItem < model.accounts.count)
            view?.showAccount(account: model.accounts[selectedAccountItem])
        }
    }
    
    var selectedAccountItem:Int = -1
    
    private var model:UserModel {
        return UserModel.currentUser!
    }
    
    var balance:String? {
        assert(selectedAccountItem >= 0 && selectedAccountItem < model.accounts.count)
        return model.accounts[selectedAccountItem].balance.value
    }
}

