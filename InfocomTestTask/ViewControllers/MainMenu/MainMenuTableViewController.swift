//
//  MainMenuTableViewController.swift
//  InfocomTestTask
//
//  Created by Admin on 05.08.2021.
//

import UIKit
import ProgressHUD

private let standardCellId = "StandardCell"

class MainMenuTableViewController: UITableViewController {
   
    private let presenter = MainMenuPresenter()
    private var sections:[MainMenuSectionModel] = []
    
    override func viewDidLoad() {
        self.tableView = UITableView(frame: .zero, style: .insetGrouped)
        super.viewDidLoad()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: standardCellId)
        //self.navigationController?.navigationBar.prefersLargeTitles = true
        
        showContent(viewController: ContentViewController.instantiateHome())
        presenter.view = self
        
        NotificationCenter.default.addObserver(forName: Notification.Name(AuthService.logoutNotification), object: nil, queue: nil) { [unowned self] (_) in
            self.showLogout()
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].menuTitle
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = sections[indexPath.section].items[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: standardCellId)!
        cell.textLabel?.text = item.menuTitle
        
        if let image = item.menuIconImage {
            let margin = 0.1 * image.size.height
            cell.imageView?.image = item.menuIconImage?.withInset(UIEdgeInsets(top: margin, left: 0, bottom: margin, right: 0))
            cell.imageView?.contentMode = .scaleAspectFit
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = sections[indexPath.section].items[indexPath.row]
        let vc = ContentViewController.instantiate(withIdentifier: item.contentIdentifier)
        (vc as? AccountViewController)?.presenter.selectedAccountItem = indexPath.row
        showContent(viewController: vc)
    }
    
    private func showContent(viewController:UIViewController) {
        
        if viewController is UINavigationController {
            self.showDetailViewController(viewController, sender: nil)
        } else {
            self.showDetailViewController(UINavigationController(rootViewController: viewController), sender: nil)
        }
    }
}

extension MainMenuTableViewController: MainMenuViewProtocol {
    
    func performMenuChanges(sections: [MainMenuSectionModel]) {
        self.sections = sections.filter { $0.items.count > 0 }
        self.tableView.reloadData()
    }
}

extension MainMenuTableViewController: LoginViewProtocol {
    
    func showLoginSuccess(user: UserModel) {
        ProgressHUD.dismiss()
        presenter.resetUserMenuItems(user: user)
        
        if SplitViewController.shared.isCollapsed {
            self.navigationController?.popToRootViewController(animated: true)
        } else {
            showContent(viewController: ContentViewController.instantiateUser())
        }
    }
    
    func showLoginFailure(error: String) {
        ProgressHUD.dismiss()
        AlertHelper.showMessage(error)
    }
    
    func showLogout() {
        presenter.removeUserMenuItems()
    
        if SplitViewController.shared.isCollapsed {
            self.navigationController?.popToRootViewController(animated: true)
        } else {
            showContent(viewController: ContentViewController.instantiateHome())
        }
    }
}
