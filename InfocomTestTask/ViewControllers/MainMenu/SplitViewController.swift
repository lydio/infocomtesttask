//
//  SplitViewController.swift
//  InfocomTestTask
//
//  Created by Admin on 06.08.2021.
//

import UIKit

class SplitViewController: UISplitViewController {
    
    static let shared = SplitViewController()
    var masterController:UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        masterController = MainMenuTableViewController()
        masterController.navigationItem.title = "Menu"
        
        self.viewControllers = [UINavigationController(rootViewController: masterController)]
        self.preferredDisplayMode = UISplitViewController.DisplayMode.allVisible
        //self.delegate = self
    }
}

/*extension SplitViewController: UISplitViewControllerDelegate {

    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        
        return true
    }
}*/
