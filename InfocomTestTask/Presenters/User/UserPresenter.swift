//
//  UserPresenter.swift
//  InfocomTestTask
//
//  Created by Admin on 07.08.2021.
//


import UIKit


protocol UserViewProtocol:class {
    func showUser(personalData:PersonalDataModel, photo:UIImage?)
}

class UserPresenter {
    
    weak var view:UserViewProtocol? = nil {
        didSet {
            view?.showUser(personalData: model.personalData, photo: model.photo)
        }
    }
    
    var model:UserModel {
        return UserModel.currentUser!
    }
    
    func logout() {
        AuthService.logout()
    }
}

