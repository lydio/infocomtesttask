//
//  TransactionHistoryViewController.swift
//  InfocomTestTask
//
//  Created by Admin on 08.08.2021.


import UIKit

class TransactionHistoryViewController: ContentViewController, UITableViewDelegate, UITableViewDataSource {
   

    let presenter = TransactionHistoryPresenter()
    private var transactions:[TransactionModel] = []
    private var currency:String = ""
    @IBOutlet private weak var tableView: UITableView!
    
    override func viewDidLoad() {
        navigationTitle = "Transaction History"
        super.viewDidLoad()
        
        presenter.view = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        transactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let transaction = transactions[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath) as! TransactionHistoryTableViewCell
        cell.createdAtLabel.text = transaction.createdAt.value!
        cell.otherAccountNumnberLabel.text = transaction.otherAccountNumber.value!
        cell.sumLabel.text = currency + transaction.sum.value!
        
        return cell
    }
}


extension TransactionHistoryViewController: TransactionHistoryViewProtocol {
    
    func showHistory(forAccount account: AccountModel) {
        
        transactions = account.transactions
        
        switch account.currency.value {
            case "dollar":
                currency = "$"
            case "euro":
                currency = "E"
            default:
                currency = ""
        }
    }
}



