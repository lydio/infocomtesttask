//
//  LoginViewController.swift
//  InfocomTestTask
//
//  Created by Admin on 07.08.2021.
//

import UIKit
import ProgressHUD

class LoginViewController: ContentViewController {

    let presenter = LoginPresenter()

    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var loginButton: UIButton!
    
    
    override func viewDidLoad() {
        navigationTitle = "Login"
        super.viewDidLoad()
        
        emailTextField.text = "user@gmail.com"
        passwordTextField.text = "1234"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        ProgressHUD.dismiss()
        presenter.cancelAllTasks()
    }
    
    @IBAction private func sendLoginForm(_ sender: Any) {
        
        guard 
            let email = emailTextField.text,
            email.isValidEmail(),
            let pasword = passwordTextField.text,
            pasword.count > 3
        else {
            AlertHelper.showMessage("Invalid password or email!")
            return
        }
        ProgressHUD.show()
        presenter.login(email: email, password: pasword)
    }
    
    @IBAction private func backgroundTap(_ sender: Any) {
       
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
}
