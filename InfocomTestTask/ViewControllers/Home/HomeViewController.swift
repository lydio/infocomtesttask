//
//  HomeViewController.swift
//  InfocomTestTask
//
//  Created by Admin on 06.08.2021.
//

import UIKit

class HomeViewController: ContentViewController {

    private let presenter = HomePresenter()
    
    override func viewDidLoad() {
        navigationTitle = "Home"
        super.viewDidLoad()
    }
    
}


extension HomeViewController: HomeViewProtocol {
    
}


