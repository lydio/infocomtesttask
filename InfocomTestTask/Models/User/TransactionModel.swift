//
//  TransactionModel.swift
//  InfocomTestTask
//
//  Created by Admin on 08.08.2021.
//

import Foundation

struct TransactionModel {
    var createdAt:FieldModel!
    var sum:FieldModel!
    var otherAccountNumber:FieldModel!
}
