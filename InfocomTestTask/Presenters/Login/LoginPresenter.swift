//
//  LoginPresenter.swift
//  InfocomTestTask
//
//  Created by Admin on 07.08.2021.
//

import Foundation


protocol LoginViewProtocol:class {
    
    func showLoginSuccess(user:UserModel)
    func showLoginFailure(error:String)
}

class LoginPresenter {
    
    weak var view:LoginViewProtocol? = nil
    
    func login(email:String, password:String) {
        
        AuthService.login(email: email, password: password) { [weak self] (user, error) in
            if let user = user {
                self?.view?.showLoginSuccess(user: user)
            } else {
                self?.view?.showLoginFailure(error: error ?? "Unknown error")
            }
        }
    }
    
    func cancelAllTasks() {
        //
    }
}
