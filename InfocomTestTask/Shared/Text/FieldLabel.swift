//
//  FieldLabel.swift
//  InfocomTestTask
//
//  Created by Admin on 08.08.2021.
//

import UIKit

@IBDesignable
class FieldLabel: UILabel {

    @IBInspectable var name:String = "name" {
        didSet {
            setup()
        }
    }
    @IBInspectable var value:String = "value" {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var nameColor: UIColor = .black {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var valueColor: UIColor = .black {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var nameFontSize: CGFloat = 17 {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var valueFontSize: CGFloat = 17 {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var separatorCount: Int = 0 {
        didSet {
            setup()
        }
    }
    

    func setup() {
        
        let nameAttributes: [NSAttributedString.Key: Any] = [
            .font: self.font.withSize(nameFontSize),
            .foregroundColor: nameColor,
        ]
        let valueAttributes: [NSAttributedString.Key: Any] = [
            .font: self.font.withSize(valueFontSize),
            .foregroundColor: valueColor,
        ]
        
        var separator = ": "
        if separatorCount > 0 {
            separator += String(repeating: " ", count: separatorCount)
        }
        let attrString = NSMutableAttributedString(string: name + separator + value)
        attrString.addAttributes(nameAttributes, range: NSRange(location: 0, length: name.count + separator.count))
        attrString.addAttributes(valueAttributes, range: NSRange(location: name.count + separator.count, length: value.count))
        
        self.attributedText = attrString
    }
    
    func clone() -> FieldLabel {
        
        let label = FieldLabel()
        label.font = font
        label.name = name
        label.value = value
        label.nameFontSize = nameFontSize
        label.valueFontSize = valueFontSize
        label.nameColor = nameColor
        label.valueColor = valueColor
        label.separatorCount = separatorCount
        
        return label
    }
    
    
    func spawn(withFields fields:[FieldModel]) -> [FieldLabel] {
        
        var s = -1
        for field in fields {
            s = max(s, field.name.count)
        }
        
        var labels = [FieldLabel]()
        for i in 0..<fields.count {
            let label = i > 0 ? clone() : self
            let field = fields[i]
            label.name = field.name
            label.value = field.value ?? "-"
            label.separatorCount = 0 //s - field.name.count
            labels.append(label)
        }
        return labels
    }
}
