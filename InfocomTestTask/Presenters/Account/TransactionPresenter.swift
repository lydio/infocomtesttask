//
//  TransactionPresenter.swift
//  InfocomTestTask
//
//  Created by Admin on 08.08.2021.
//

import Foundation


protocol TransactionViewProtocol:class {
    func fillForm(forAccount account:AccountModel)
    func showTransactionSuccess(user:TransactionModel)
    func showTransactionFailure(error:String)
}

class TransactionPresenter {
    
    weak var view:TransactionViewProtocol? = nil {
        didSet {
            assert(selectedAccountItem >= 0 && selectedAccountItem < model.accounts.count)
            view?.fillForm(forAccount: model.accounts[selectedAccountItem])
        }
    }
    
    var selectedAccountItem:Int = -1
    
    private var model:UserModel {
        return UserModel.currentUser!
    }
    
    func cancelAllTasks() {
        //
    }
    
    func checkAccountNumberInput(accountNumber:String?) -> Bool {
        
        guard var accountNumber = accountNumber else {
            return false
        }
        accountNumber = accountNumber.filter("0123456789.".contains)
        return accountNumber.count == 16
    }
    
    func sendTransactionRequest(accountNumber: String, otherAccountNumber:String, sum:String) {
        
        TransactionService.sendTransactionRequest(accountNumber: accountNumber, otherAccountNumber: otherAccountNumber, sum: sum) { [weak self] (transaction, error) in
            
            if let transaction = transaction {
                self?.view?.showTransactionSuccess(user: transaction)
            } else {
                self?.view?.showTransactionFailure(error: error ?? "Unknown error")
            }
        }
    }
}
