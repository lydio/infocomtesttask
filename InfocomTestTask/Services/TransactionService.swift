//
//  TransactionService.swift
//  InfocomTestTask
//
//  Created by Admin on 08.08.2021.
//

import Foundation


class TransactionService {
    
    static func sendTransactionRequest(accountNumber: String, otherAccountNumber:String, sum:String, completion: @escaping (TransactionModel?, String?)->()) {
        
        //TODO: Network manager
        DispatchQueue.global().asyncAfter(deadline: .now() + 1.0) {
            let transaction = UserModel.currentUser?.executeTestTransaction(accountNumber: accountNumber, otherAccountNumber: otherAccountNumber, sum: sum)
            
            DispatchQueue.main.async {
                if let transaction = transaction {
                    completion(transaction, nil)
                } else {
                    completion(nil, "Transaction failed")
                }
            }
        }
    }
}
