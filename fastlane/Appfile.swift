var appIdentifier: String { return "com.info.testtask1" } // The bundle identifier of your app
var appleID: String { return "pleasure2deal.com@gmail.com" } // Your Apple email address


var itcTeam: String? { return "118367005" } // App Store Connect Team ID
var teamID: String { return "5F99YX6B8Z" } // Apple Developer Portal Team ID

/* Использование:
 uploadToTestflight(
    username: OptionalConfigValue(stringLiteral: appleID),
    appIdentifier: OptionalConfigValue(stringLiteral: appIdentifier),
    teamId: itcTeam,
    devPortalTeamId: OptionalConfigValue(stringLiteral: teamID)
 )
*/
// For more information about the Appfile, see:
//     https://docs.fastlane.tools/advanced/#appfile
